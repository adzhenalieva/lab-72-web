import React, {Component} from 'react';
import {Switch, Route} from "react-router-dom";
import Menu from "./containers/Menu/Menu";
import DishAdd from "./components/DishAdd/DishAdd";
import DishEdit from "./components/DishEdit/DishEdit";
import Layout from "./components/Layout/Layout";
import Orders from "./containers/Orders/Orders";

import './App.css';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Layout>
                    <Switch>
                        <Route path="/" exact component={Menu}/>
                        <Route path="/dishes/add" exact component={DishAdd}/>
                        <Route path="/dishes/:id/edit" component={DishEdit}/>
                        <Route path="/dishes/orders" component={Orders}/>
                    </Switch>
                </Layout>
            </div>
        );
    }
}

export default App;
