import React, {Component} from 'react';

import DishesList from "../../components/DishesList/DishesList";
import Spinner from "../../components/UI/Spinner/Spinner";
import {connect} from "react-redux";
import {dishDelete, fetchDishes} from "../../store/actions/dishesCombine";

import './Menu.css';


class Menu extends Component {

    componentDidMount() {
        this.props.fetchDishes();
    }


    goToEdit = (id) => {
        this.props.history.push({
            pathname: '/dishes/' + id + '/edit'
        });
    };

    delete = (id) => {
        this.props.dishDelete(id);
    };

    render() {
        let dishes = null;
        if (this.props.dishes) {
            dishes = this.props.dishes.map((dish) => (
                <DishesList key={dish.id}
                            dishName={dish.dishName}
                            price={dish.price}
                            image={dish.image}
                            goToEdit={() => this.goToEdit(dish.id)}
                            delete={() => this.delete(dish.id)}>
                </DishesList>
            ))
        }

        return (
            this.props.loading ?
                <Spinner/> :
                <div className="Menu">
                    <div className="Dishes">
                        {dishes}
                    </div>
                </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        dishes: state.dc.dishes,
        loading: state.dc.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchDishes: () => dispatch(fetchDishes()),
        dishDelete: id => dispatch(dishDelete(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Menu);