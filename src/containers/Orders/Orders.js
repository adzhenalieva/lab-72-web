import React, {Component} from 'react';
import './Orders.css';

import {connect} from "react-redux";
import {fetchOrders, orderDelete} from "../../store/actions/orderAction";
import {fetchDishes} from "../../store/actions/dishesCombine";

class Orders extends Component {

    componentDidMount() {
        this.props.fetchDishes();
        this.props.fetchOrders();
    }

    render() {

        if (!this.props.dishes || !this.props.orders) {
            return <div className="Orders">There is no orders here yet</div>
        }
        const orders = Object.keys(this.props.orders).map(orderId => {
            const id = orderId;

            let order = this.props.orders[orderId].positions;
            let deliveryPrice = 150;
            let totalPrice = 0;

            let orderItems = Object.keys(order).map((dishId, index) => {
                const dish = dishId;
                const orderQty = order[dishId];
                let orderPrice = 0;

                for (let i = 0; i < this.props.dishes.length; i++) {
                    if (this.props.dishes[i].dishName === dishId) {
                        orderPrice = this.props.dishes[i].price * orderQty;
                        totalPrice += orderPrice;
                    }
                }

                return (
                    <div key={index}>
                        <span>{dish}: x {orderQty}</span>
                        <span>{orderPrice} KGS</span>
                    </div>
                )

            });
            return (
                <div className="Wrap" key={orderId}>
                    <h4>Order ID: {orderId}</h4>
                    <div className="OrderItem">
                        <div className="OrderItems">
                            {orderItems}
                        </div>
                        <div>
                            <p><strong>Total price: </strong>{totalPrice + deliveryPrice} KGS</p>
                            <button className="Button" onClick={() => this.props.orderDelete(id)}>Complete order</button>
                        </div>
                    </div>
                </div>
            )
        });

        return (
            <div className="Orders">
                {orders}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        dishes: state.dc.dishes,
        orders: state.order.orders
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchDishes: () => dispatch(fetchDishes()),
        fetchOrders: () => dispatch(fetchOrders()),
        orderDelete: id => dispatch(orderDelete(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Orders);