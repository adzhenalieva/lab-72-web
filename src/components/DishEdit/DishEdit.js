import React, {Component} from 'react';
import DishForm from "../DishesForm/DishForm";
import {connect} from "react-redux";
import {getDataForEdit, sendDataForEdit} from "../../store/actions/dishesCombine";
import Spinner from "../../components/UI/Spinner/Spinner";

import './DishEdit.css';


class DishEdit extends Component {

    componentDidMount() {
        this.props.getDataForEdit(this.props.match.params.id);
    }

    edit = dish => {
        this.props.edit(this.props.match.params.id, this.props.history, dish)
    };

    render() {
        let dish = <DishForm onSubmit={this.edit}
                             dish={this.props.dish}/>;
        if (!this.props.dish) {
            dish = <Spinner/>
        }
        return (
            <div className="DishEdit">
                <h2>Edit dish</h2>
                {dish}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        dish: state.dc.dishById
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getDataForEdit: id => dispatch(getDataForEdit(id)),
        edit: (id, history, dish) => dispatch(sendDataForEdit(id, history, dish))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(DishEdit);
