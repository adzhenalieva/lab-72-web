import React, {Component} from 'react';
import {connect} from "react-redux";
import DishForm from "../DishesForm/DishForm";
import {dishPost} from "../../store/actions/dishesCombine";

import './DishAdd.css';


class DishAdd extends Component {
    addDish = dish => {
        this.props.dishPost(this.props.history, dish);
    };

    render() {
        return (
            <div className="DishAdd">
                <h1>Add new dish</h1>
                <DishForm onSubmit={this.addDish}/>
            </div>
        );
    }
}
const mapDispatchToProps = () => {
    return {
        dishPost: (history, dish) => dishPost(history, dish)
    }
};

export default connect(mapDispatchToProps)(DishAdd);