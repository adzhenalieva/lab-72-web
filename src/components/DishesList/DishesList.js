import React from 'react';
import './DishesList.css';

const DishesList = props => {
    return (
        <div className="DishesList">
            <img className="DishImage" src={props.image} alt="dish"/>
            <p>{props.dishName}</p>
            <p className="Price">Price: {props.price} KGS</p>
            <button className="Button" onClick={props.goToEdit}>Edit</button>
            <button className="Button" onClick={props.delete}>Delete</button>
        </div>
    );
};

export default DishesList;