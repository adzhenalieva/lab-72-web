import {DISH_EDIT_SUCCESS, DISH_EDITED, DISH_FAILURE, DISH_REQUEST, DISH_SUCCESS,} from "../actions/actionTypes";

const initialState = {
    dishes: [],
    loading: true,
    dishById: null
};

const dishesCombineReducer = (state = initialState, action) => {
    switch (action.type) {
        case DISH_SUCCESS:
            return {
                ...state,
                dishes: action.response, loading: false
            };
        case DISH_REQUEST:
            return {
                ...state, loading: true
            };
        case DISH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case DISH_EDITED:
            return {
                ...state,
                dishById: null,
                error: action.error
            };
        case DISH_EDIT_SUCCESS:
            return {
                ...state,
                dishById: action.response, loading: false
            };
        default:
            return state;
    }
};
export default dishesCombineReducer;