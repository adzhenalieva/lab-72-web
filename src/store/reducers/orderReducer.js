import {
    ORDER_FAILURE,
    ORDER_REQUEST,
    ORDER_SUCCESS
} from "../actions/actionTypes";



const initialState = {
    orders: [],
    error: null
};

const orderReducer = (state = initialState, action) => {
    switch (action.type) {
        case ORDER_REQUEST:
            return {...state};
        case ORDER_SUCCESS:
            return {...state, orders: action.response};
        case ORDER_FAILURE:
            return {
                ...state,
                error: action.error
            };
        default:
            return state;
    }
};


export default orderReducer;