import {DISH_EDIT_SUCCESS, DISH_EDITED, DISH_FAILURE, DISH_REQUEST, DISH_SUCCESS} from "./actionTypes";
import axios from '../../axios-dishes';

export const dishRequest = () => ({type: DISH_REQUEST});
export const dishSuccess = response => ({type: DISH_SUCCESS, response});
export const dishFailure = error => ({type: DISH_FAILURE, error});
export const dishEditSuccess = response => ({type: DISH_EDIT_SUCCESS, response});
export const dishEdited = () => ({type: DISH_EDITED});


export const fetchDishes = () => {
    return dispatch => {
        dispatch(dishRequest());
        axios.get('/dishes.json').then(response => {
            const dishes = Object.keys(response.data).map(id => {
                return {...response.data[id], id}
            });
            dispatch(dishSuccess(dishes));
        }, error => {
            dispatch(dishFailure(error));
        });
    }
};

export const getDataForEdit = (id) => {
    return dispatch => {
        axios.get('dishes/' + id + '.json').then(response => {
            dispatch(dishEditSuccess(response.data))
        }, error => {
            dispatch(dishFailure());
        });
    };
};

export const sendDataForEdit = (id, history, dish) => {
    return dispatch => {
        axios.put('dishes/' + id + '.json', dish).then(() => {
            dispatch(dishEdited());
            history.push('/');
        }, error => {
            dispatch(dishFailure());
        });
    };
};


export const dishPost = (history, dish) => {
    axios.post('dishes.json', dish).then(() => {
        history.replace('/');
    });
};


export const dishDelete = (id) => {
    return dispatch => {
        axios.delete('/dishes/' + id + '.json').then(() => {
            dispatch(fetchDishes());
        })
    };
};