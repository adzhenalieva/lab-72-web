export const ORDER_REQUEST = 'ORDER_REQUEST';
export const ORDER_SUCCESS = 'ORDER_SUCCESS';
export const ORDER_FAILURE = 'ORDER_FAILURE';

export const DISH_REQUEST = 'DISH_REQUEST';
export const DISH_SUCCESS = 'DISH_SUCCESS';
export const DISH_FAILURE = 'DISH_FAILURE';

export const DISH_EDIT_SUCCESS = 'DISH_EDIT_SUCCESS';
export const DISH_EDITED = 'DISH_EDITED';