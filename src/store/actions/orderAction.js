import {ORDER_FAILURE, ORDER_REQUEST, ORDER_SUCCESS} from "./actionTypes";
import axios from '../../axios-dishes';

export const orderRequest = () => ({type: ORDER_REQUEST});

export const orderSuccess = response => ({type: ORDER_SUCCESS, response});

export const orderFailure = error => ({type: ORDER_FAILURE, error});

export const fetchOrders = () => {
    return dispatch => {
        dispatch(orderRequest());
        axios.get('/dishesOrders.json').then(response => {
            dispatch(orderSuccess(response.data));
        }, error => {
            dispatch(orderFailure(error));
        });
    }
};

export const orderDelete = (id) => {
    return dispatch => {
        axios.delete('/dishesOrders/' + id + '.json').then(() => {
            dispatch(fetchOrders());
        })
    };
};